package br.ucsal.ads.so2020;

public class Armazem {

	private int conteudo;
	private boolean disponivel = false;

	public synchronized int get (int numero) {
		while (disponivel == false ) {
			try {
				wait();
			}catch (InterruptedException e) {

			}
		}

		disponivel = false;
		System.out.println("Consumidor comprou: " + conteudo);
		notifyAll();
		return conteudo;


	}
	public synchronized void put(int numero, int valor) {
		while (disponivel == true ) {
			try {
				wait();
			}catch (InterruptedException e) {

			}
		}
		conteudo = valor;
		disponivel = true;
		System.out.println("Produtor armazenou: "+ conteudo);
		notifyAll();
		

	}

}
