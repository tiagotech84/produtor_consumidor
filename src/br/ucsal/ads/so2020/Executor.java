package br.ucsal.ads.so2020;

public class Executor {
	
	public static void main(String[] args) {
		Armazem a = new Armazem();
		Produtor p = new Produtor(a, 0);
		Consumidor c = new Consumidor(a, 0);
		
		p.start();
		c.start();
		
		
	}

}
