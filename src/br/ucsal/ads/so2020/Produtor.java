package br.ucsal.ads.so2020;

public class Produtor extends Thread{
	
	private Armazem armazem;
	private int numero;
	
	public Produtor (Armazem a, int numero) {
		armazem = a;
		this.numero = numero;
	}
	
	public void run() {
		for (int i = 1; i <= 30; i++) {
			armazem.put(numero, i);
			try {
				sleep((int)(Math.random()*1000));
			}catch (InterruptedException e) {
			}
		}
	}

}
